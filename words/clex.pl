mn_sg(kg, kg).
noun_mass(age, age, neutr).
adv(always, always).
prep(with, with).
adj_tr('fond-of', 'fond-of', of).
dv_finsg(writes, write, '').
noun_pl(companies, company, neutr).
noun_pl(headphones, headphone, neutr).
noun_sg(company, company, neutr).
noun_sg(headphone, headphone, neutr).
pn_sg('Apple-Inc', 'Apple-Inc', neutr).
pn_sg('Beats', 'Beats', neutr).
iv_finsg(produces, produce).
tv_finsg(produces, produce).
iv_finsg(agrees, agree).
tv_finsg(agrees, agree).
adj_itr(beautiful, beautiful).
adj_itr_comp('more beautiful', beautiful).
adj_itr_sup('most beautiful', beautiful).
