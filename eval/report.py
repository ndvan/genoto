import argparse
from IPython import embed
import json
import os
from pprint import pprint
from decimal import *

# initiate the parser
parser = argparse.ArgumentParser(description='Report')
parser.add_argument('resource', metavar="RESOURCE",
                    help='folder containing score')
args = parser.parse_args()  

bleu_score = []
rouge_score = []

script_dir = os.path.dirname(__file__)
abs_folder_path = os.path.join(script_dir, args.resource)
for filename in os.listdir(abs_folder_path):
  abs_file_path = os.path.join(script_dir, args.resource, filename)
  with open(abs_file_path) as f:
    data = json.load(f)
  for c in data["candidates"]:
    bleu_score.append(Decimal(c["bleu"]))
    rouge_score.append(c["rouge"][0])
    
bleu_validable = list(filter(lambda x: x > Decimal(0.0001), bleu_score))
# print(bleu_score)

rouge_1_f = list(map(lambda x: x["rouge-1"]["f"], rouge_score))
rouge_2_f = list(map(lambda x: x["rouge-2"]["f"], rouge_score))
rouge_l_f = list(map(lambda x: x["rouge-l"]["f"], rouge_score))

print("Number of sentences:           %d" % len(bleu_score))
print("Number of validable sentences: %d" % len(bleu_validable))
print("Avarage bleu score:            %f" % (sum(bleu_validable) / len(bleu_validable)))

print("F Rouge-1:                     %f" % (sum(rouge_1_f) / len(rouge_1_f)))
print("F Rouge-2:                     %f" % (sum(rouge_2_f) / len(rouge_2_f)))
print("F Rouge-l:                     %f" % (sum(rouge_l_f) / len(rouge_l_f)))
