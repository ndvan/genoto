# two references for one document
from nltk.translate.bleu_score import corpus_bleu
from nltk.translate.bleu_score import sentence_bleu
from nltk.tokenize import sent_tokenize
from nltk.parse.corenlp import CoreNLPParser
from nltk.stem.wordnet import WordNetLemmatizer
# include standard modules
import argparse
from IPython import embed
import json
import os
from pprint import pprint
from rouge import Rouge 

def main():
  # initiate the parser
  parser = argparse.ArgumentParser(description='Find BLEU-4 score.')
  parser.add_argument('resource', metavar="RESOURCE",
                      help='json file containing references and candidates')
  args = parser.parse_args()  

  with open(args.resource) as f:
    data = json.load(f)

  # pprint(data)
  
  rouge = Rouge()

  references = []

  outdata = {}
  outdata["references"] = data["references"]
  outdata["candidates"] = []
  
  parser = CoreNLPParser()
  
  for s in sent_tokenize(data["references"]):
    references.append( list(parser.tokenize(s)) )
  
  for s in sent_tokenize(data["candidates"]):
    candidates = list(parser.tokenize(s))
    evaluation = {}
    evaluation["sent"]  = s
    evaluation["bleu"]  = sentence_bleu(references, candidates, weights=(0.33,0.33,0.33,0))
    evaluation["rouge"] = rouge.get_scores(s, data["references"])
    outdata["candidates"].append(evaluation)
  
  filename, file_extension = os.path.splitext(os.path.basename(args.resource))
  script_dir = os.path.dirname(__file__)
  abs_file_path = os.path.join(script_dir, 'results/fooddrink/%s.json' % filename)
  
  with open(abs_file_path, 'w') as outfile:
    json.dump(outdata, outfile, indent=2, sort_keys=True)
    
if __name__ == "__main__":
  main()
