import re

class Annotation:
    prefix     = "# @"
    p_noun     = "n"
    p_pronoun  = "pn"
    p_sentence = "lin"
    
    def __init__(self, l):
        self.raw = l

    def remove_marker(self):
        return re.sub(Annotation.prefix + '.+: ', '', self.raw).replace("\n", "")
        
    @classmethod
    def is_noun(cls, l):
        return l.strip().startswith(cls.prefix + cls.p_noun)
        # another implementation is checking right after line in ontology whether it is class()

    @classmethod
    def is_pronoun(cls, l):
        return l.strip().startswith(cls.prefix + cls.p_pronoun)
        # another implementation is checking right after line in ontology whether it is instanceOf()

    @classmethod
    def is_sentence(cls, l):
        return l.strip().startswith(cls.prefix + cls.p_sentence)
        # another implementation?