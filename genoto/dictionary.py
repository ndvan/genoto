import requests
from bs4 import BeautifulSoup
from IPython import embed
import re
import json

def plural(w):
    try:
        r = requests.post("http://tools.dehumanizer.com/plural/index2.php", data={'texto': w})
        html = r.text
        parsed_html = BeautifulSoup(html, "html.parser")
        return parsed_html.body.select('div#main > h3 > pre')[0].text.replace("\n", "")
    except Exception: 
        print 'Can not find plural form of "{}"'.format(w)
    
def verb_category(v):
    try:
        r = requests.get("http://www.yourdictionary.com/" + v)
        html = r.text
        parsed_html = BeautifulSoup(html, "html.parser")
        return list( map(lambda x: x.text.strip(), parsed_html.body.select('.pos')) )
    except Exception: 
        print 'Can not find plural form of "{}"'.format(v)
    
def verb_finsi(v):
    try:
        r = requests.get("http://conjugator.reverso.net/conjugation-english-verb-" + v + ".html")
        html = r.text
        parsed_html = BeautifulSoup(html, "html.parser")
        list_i = parsed_html.body.select('.indicative-wrap div')[0].select('i')
        for ind, val in enumerate(list_i):
            if val.text == "he/she/it ":
                return list_i[ind + 1].text
    except Exception:
        print 'Can not find finite singular form of "{}"'.format(v)

def syllable(w):
    try:
        # free 10000 words/day
        r = requests.get("https://api.datamuse.com/words?sp=" + w + "&md=s")
        word_data = json.loads( r.text )
        return word_data[0]["numSyllables"]
    except Exception:
        print 'Can not query number of syllable of "{}"'.format(w)
      
def comparative(adj):
    try:
        syllables = syllable(adj)
        if syllables > 2:
            return "more " + adj
        else:
            return raw_input("Please enter comparative form of " + adj + ": ")
    except Exception as e:
        print e
        print 'Can not find comparative form of "{}"'.format(adj)
    
def superlative(adj):
    try:
        syllables = syllable(adj)
        if syllables > 2:
            return "most " + adj
        else:
            return raw_input("Please enter superlative form of " + adj + ": ")
    except Exception as e:
        print e
        print 'Can not find superlative form of "{}"'.format(adj)
