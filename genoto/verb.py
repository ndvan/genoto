from . import dictionary
from nltk.stem.wordnet import WordNetLemmatizer
from IPython import embed

class Verb:

    def __init__(self, v):
        lemmatizer = WordNetLemmatizer()
        self.v = lemmatizer.lemmatize(v, 'v')

        self.t = 0
        self.i = 0
        self.d = 0
        
        cats = dictionary.verb_category(self.v)
        for c in cats:
            if c == "transitive":
                self.t = 1
            elif c == "intransitive":
                self.i = 1
            elif c == "ditransitive":
                self.d = 1
        
        self.finite_singular = dictionary.verb_finsi(self.v)
