from . import dictionary

class Adj:

    def __init__(self, adj):
        self.pos = adj
        self.com = dictionary.comparative(adj)
        self.sup = dictionary.superlative(adj)