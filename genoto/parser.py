import os
from .annotation import Annotation
from .noun import Noun
from .pronoun import Pronoun
from .sentence import Sentence

class Parser:
    
    def __init__(self, path):
        self.path = path
        # self.__potential_psychic = lp

    def files(self, ext):
        files = []
        for file in os.listdir(self.path):
            if file.endswith("." + ext):
                files.append( file )
        return files
  
    def extract(self, file):
        abpath = os.path.join(self.path, file)
        f = open(abpath, "r")
        annotations = []
        nouns = []
        pronouns = []
        
        for line in f:
            if Annotation.is_noun(line):
                a = Annotation(line)
                annotations.append(a)
                
                n = Noun(a.remove_marker().lower())
                nouns.append(n)
                
            elif Annotation.is_pronoun(line):
                a = Annotation(line)
                annotations.append(a)
                
                pn = Pronoun(a.remove_marker())
                pronouns.append(pn)
            
            elif Annotation.is_sentence(line):
                a = Annotation(line)
                annotations.append(a)
                
                s = Sentence(a.remove_marker())
                
                    
        return nouns, pronouns
