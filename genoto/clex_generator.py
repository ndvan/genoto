class ClexGenerator:

    def __init__(self, nouns, pronouns, verbs, adjectives):
        self.nouns      = nouns
        self.pronouns   = pronouns
        self.verbs      = verbs
        self.adjectives = adjectives
        # self.__potential_psychic = lp
        
    def gen(self, clex_path_arg=None):
        if clex_path_arg == None:
            clex_path = "APE/lexicon/clex_lexicon.pl"
        else:
            clex_path = clex_path_arg
            
        with open(clex_path,"w") as the_file:
            the_file.write("mn_sg(kg, kg).\n")
            the_file.write("noun_mass(age, age, neutr).\n")

            the_file.write("adv(always, always).\n")

            the_file.write("prep(with, with).\n")

            the_file.write("adj_tr('fond-of', 'fond-of', of).\n")

            the_file.write("dv_finsg(writes, write, '').\n")

            for n in self.nouns:
                the_file.write("noun_pl(" + n.pl + ", " + n.sg + ", neutr).\n")
            
            for n in self.nouns:
                the_file.write("noun_sg(" + n.sg + ", " + n.sg + ", neutr).\n")

            for p in self.pronouns:
                the_file.write("pn_sg('" + p.sg + "', '" + p.sg + "', neutr).\n")
            
            for v in self.verbs:
                if v.i == 1:
                    the_file.write("iv_finsg(" + v.finite_singular + ", " + v.v + ").\n")
                if v.t == 1:
                    the_file.write("tv_finsg(" + v.finite_singular + ", " + v.v + ").\n")
                if v.d == 1:
                    the_file.write("dv_finsg(" + v.finite_singular + ", " + v.v + ", '').\n")
                    
            for a in self.adjectives:
                the_file.write("adj_itr(" + a.pos + ", " + a.pos + ").\n")
                the_file.write("adj_itr_comp('" + a.com + "', " + a.pos + ").\n")
                the_file.write("adj_itr_sup('" + a.sup + "', " + a.pos + ").\n")
                
    def gen_interface(self, interface_path_arg):
        with open(interface_path_arg,"w") as the_file:
            the_file.write("--# -path=.:present\nincomplete concrete TestAttemptoI of TestAttempto = AttemptoAce ** open Oper in {\nlin\n")

            # the_file.write("mn_sg(kg, kg).\n")
#             the_file.write("noun_mass(age, age, neutr).\n")
#
#             the_file.write("adv(always, always).\n")
#
#             the_file.write("prep(with, with).\n")
#
#             the_file.write("adj_tr('fond-of', 'fond-of', of).\n")
#
#             the_file.write("dv_finsg(writes, write, '').\n")
#
            for n in self.nouns:
                the_file.write(n.sg + "_N = aceN \"" + n.sg + "\" ;\n")
    
            for p in self.pronouns:
                the_file.write(p.sg.replace("-", "_") + "_PN = acePN \"" + p.sg + "\" ;\n")
                
            for v in self.verbs:
                if v.t == 1:
                    the_file.write(v.v + "_V2 = aceV2 \"" + v.v + "\" \"" + v.finite_singular + "\" \"" + v.v + "\";\n")
                
            for a in self.adjectives:
                the_file.write(a.pos + "_A = mkA \"" + a.pos + "\" \"" + a.com + "\";\n")

            the_file.write("}")
