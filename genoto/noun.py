from . import dictionary

class Noun:

    def __init__(self, sg):
        self.sg = sg
        self.pl = dictionary.plural(sg)