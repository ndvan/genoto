import subprocess
import re
import random 
from IPython import embed
from genoto_v2.atom import Atom

class Dependency:
  
  def __init__(self, dep, pos):
    self.dep = dep
    self.pos = pos
    self._core_args = ["nsubj", "nsubjpass", "auxpass", "cop", "obj", "iobj", "csubj", "ccomp", "xcomp", "dobj", "acl"]
    self._infer_structure_file = "structure.lp"
    self.structure = 0
    self._core_only_to_asp = False
    self._all_to_asp = False
    
  def _select_structure(self, atoms):
    pool = {}
    best = 0
    for a in atoms:
      t = Atom(a)
      if t.name() == "structure":
        candidate_match = int(t.args()[1])
        pool[ int(t.args()[0]) ] = candidate_match
        best = candidate_match if candidate_match > best else best

    return {k: v for k, v in pool.items() if v == best}
  
  def _to_asp(self, core_only):
    def token_index(s): 
      number = re.findall(r"[-+]?\d*\.\d+|\d+",s)
      if len(number) == 1:
        return number[0]
      else:
        return number[-1]
    
    def pos_tag_aspize(p):
      return p.replace("-","").replace(".","punct").replace(",","punct").replace("$","").replace(":","punct").lower()
    
    # if already encoded core only to asp, then do nothing
    if core_only:
      if self._core_only_to_asp:
        return 
      else:
        self._core_only_to_asp = True
        self._all_to_asp = False
    else:
      if self._all_to_asp:
        return
      else:
        self._all_to_asp = True
        self._core_only_to_asp = False
        
    atoms = []
    
    # encode dependency tree
    for d in self.dep:
      # if the atoms is for stop of sentence, dont encode in asp
      if "." in d:
        next
        
      s = re.findall(r"\w+\.\w+|\w+", d.replace("-","_").replace("'",""))
      if core_only:
        if s[0] in self._core_args:
          if len(s) == 4:
            atoms.append( "%s(%s,%s)." % (s[0], token_index(s[2]), token_index(s[3])) )
          else:
            atoms.append( s[0] + "(" + token_index(s[1]) + "," + token_index(s[2]) + ")." )
      else:
        if s[0] in ["ROOT"]:
          next
        elif s[0] in ["punct"]:
          atoms.append( "%s(%s,%s)." % (s[0], token_index(s[1]), token_index(s[2])) )
        elif s[0] in ["nmod"]:
          if len(s) == 4:
            atoms.append( "%s(%s,%s)." % (s[0], token_index(s[2]), token_index(s[3])) )
            atoms.append( "%s_%s(%s,%s)." % (s[0],s[1], token_index(s[2]), token_index(s[3])) )
          else:
            atoms.append( "%s(%s,%s)." % (s[0], token_index(s[1]), token_index(s[2])) )
        else:
          # print(s)
          if len(s) == 4:
            # try:
            atoms.append( "%s(%s,%s)." % (s[0], token_index(s[2]), token_index(s[3])) )
            # except:
              # embed()
          else:
            atoms.append( "%s(%s,%s)." % (s[0], token_index(s[1]), token_index(s[2])) )
    
    # encode pos-tag
    for index, p in enumerate(self.pos):
      atoms.append( "pos_tag(%s,%s)." % (index+1, pos_tag_aspize(p[1])) )
    
    return atoms
  
  def _infer(self, core_only, file):
    facts = self._to_asp(core_only)
    if facts:      
      with open("genoto_v2/temp.lp", "w") as f:
        f.write(" ".join( facts ))
      
    p = subprocess.Popen("./clingo1facts temp.lp %s" % file, cwd="genoto_v2", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for stdout_line in p.stdout:
      print(stdout_line)
  
    return stdout_line.decode().strip().split(" ")
        
  def find_structure(self):  
    atoms = self._infer(True, self._infer_structure_file)
    structure = self._select_structure(atoms)
    
    if not structure:
      print( "Can not find a suitable structure for the sentence" )
      select_structure = 0
    elif len(structure.keys()) > 1:
      print( "There are multiple structures that can be applied" )
      select_structure = random.choice(list(structure.keys()))
      print( "Select %s" % select_structure )
    else:
      select_structure = list(structure.keys())[0]
    
    self.structure = select_structure
    return select_structure
  
  def extract_main_components(self):
    return self._infer(True, "components_for_%s.lp" % str(self.structure))
  
  def extract_complement(self, comp, comp_type):
    print(comp)
    print(comp_type)
    return self._infer(False, "complement_for_%s.lp -c pos=%s" % (comp_type,comp))
    