import subprocess
import re
from IPython import embed

import string
import random
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
from pattern.en import pluralize, singularize
from genoto_v2.atom import Atom

class Constructor:
  
  _m = {
    "sub": "noun", 
    "obj": "noun", 
    "verb": "verb", 
    "verb_1": "verb", 
    "verb_2": "verb",
    "noun_compound": "noun",
    "preposition": "noun",
    "adj_mod": "adj",
    "adj": "adj",
    "noun_conjunction": "noun"
  }
  
  _pref_map = {
    "above"   : "above_Prep",
    "after"   : "after_Prep",
    "before"  : "before_Prep",
    "behind"  : "behind_Prep",
    "during"  : "during_Prep",
    "for"     : "for_Prep",
    "from"    : "from_Prep",
    "in"      : "in_Prep",
    "on"      : "on_Prep",
    "'s"      : "possess_Prep",
    "of"      : "possess_Prep",
    "through" : "through_Prep",
    "to"      : "to_Prep",
    "under"   : "under_Prep",
    "with"    : "with_Prep",
    "without" : "without_Prep"
  }
  
  def __init__(self, select_structure, main_comps, dependency, pos, tokens):
    self._select_structure = select_structure
    self._main_comps = main_comps
    self._dep = dependency
    self._tokens = tokens
    self._pos = pos
    self._dependent_tree = {}
    
  def _find_complement(self,m):
    def comp_type(comp): 
      return Constructor._m[comp] if comp in Constructor._m.keys() else ""
    
    a = Atom(m)
    ctype = a.name()
    index = a.args()[0]
    return int(index), self._dep.extract_complement(index, comp_type(ctype))
  
  def _build_dependent_tree(self, m):
    if not m: return
      
    m_index, complement = self._find_complement(m)
    if complement[0]:
      self._dependent_tree[m_index] = complement
    else:
      self._dependent_tree[m_index] = []
      
    for c in complement:
      self._build_dependent_tree(c)
 
  def _modify_dependent_tree(self):
    temp = []
    flag = False
    
    for m in self._main_comps:
      component_atom = Atom(m)
      component_index = component_atom.args()[0]
      
      if ((component_atom.name() in ["verb_2"]) or 
        (self._select_structure == 2 and component_atom.name() in ["verb"])):
        # if there is any proposition attach to V2 verb, move it to obj
        dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(component_index)]))
        
        for index, atom in enumerate(dependent_atoms):
          if atom.name() == "preposition":
            flag = True
            temp.append( self._dependent_tree[int(component_index)][index] )
    
    if flag:
      for m in self._main_comps:
        component_atom = Atom(m)
        component_index = component_atom.args()[0]
        if component_atom.name() in ["obj"]:
          print(temp)
          self._dependent_tree[int(component_index)].extend(temp)
  
  def _oper_for_noun(self, n):
    if n.isdigit():
      oper = "number_%s_N" % n.replace(" ", "_")
      return [oper + " = mkN \"" + n + "\" \"" + n + "\" ;"], oper
    elif n[0].isupper():
      oper = "%s_N" % n.replace(" ", "_").replace(".", "_").replace("-", "_")
      return [oper + " = mkN \"" + n + "\" \"" + n + "\" ;"], oper
    else:
      oper = "%s_N" % n.replace(" ", "_").replace(".", "_").replace(".", "_")
      return [oper + " = mkN \"" + n + "\" \"" + pluralize(n) + "\" ;"], oper

  def _oper_for_adj(self, adj_token):
    oper = []
    adj = adj_token.replace("-", "_")
    if adj[0].isdigit():
      adj = "a%s" % adj
    oper.append( "%s_A = mkA \"%s\" ;" % (adj, adj_token) )
    oper.append( "%s_AP = mkAP %s_A ;" % (adj, adj) )

    return oper, "%s_AP" % adj
  
  def _oper_for_adv(self, adv_token):
    oper = []
    adv = adv_token.replace("-", "_")
    oper.append( "%s_AdA = mkAdA \"%s\" ;" % (adv, adv_token) )

    return oper, "%s_AdA" % adv
  
  def _oper_for_verb(self, v):
    if self._select_structure == 1:
      return self._oper_for_verb_vv(v)
    elif self._select_structure in [2,10001]:
      return self._oper_for_verb_v2(v)
  
  def _oper_for_verb_vv(self, v):
    oper = "%s_V" % v.replace(" ", "_")
    return [oper + " = mkV \"%s\" ;" % v], "%s_V" % v
      
  def _oper_for_verb_v2(self, v):
    oper = "%s_V2" % v.replace(" ", "_")
    return [oper + " = mkV2 \"%s\" ;" % v], "%s_V2" % v
      
  def _oper_for_adj_noun(self, adj_mod_atoms, n):
    adjs_index = list(map(lambda x: int(x.args()[0]), adj_mod_atoms))
    adjs_index.sort()
    natural_order_adjs = list(map(lambda x: self._tokens[x - 1], adjs_index))
    adjs = natural_order_adjs[::-1]
    oper = []
    
    for ind,adj in enumerate(reversed(adj_mod_atoms)):
      adj_token = self._tokens[int(adj.args()[0]) - 1]
      # o, oper_adj_name = self._oper_for_adj(adj_token)
      # oper.extend(o)
      
      dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(adj.args()[0])]))
      adverbial_modifier_atoms = list(filter(lambda x: x.name() == "adverbial_modifier", dependent_atoms))
      
      o, concrete_func = self._serialize_adj(adj_token)
      oper.extend(o)
      
      if adverbial_modifier_atoms:
        o, concrete_func = self._serialize_adv_adj(adverbial_modifier_atoms, adj_token)
        oper.extend(o)
      
      if ind == 0:
        previous_cumulative_oper_name = "%s_N" % n.replace(" ", "_")
        adj_series = adjs[0].replace("-","_")
        if adj_series[0].isdigit():
          adj_series = "a%s" % adj_series
        cumulative_oper = "%s_%s_CN = " % (adj_series, n.replace(" ", "_"))
      else:
        adj_series = "_".join(reversed(adjs[:ind])).replace("-","_")
        if adj_series[0].isdigit():
          adj_series = "a%s" % adj_series
        previous_cumulative_oper_name = "%s_%s_CN" % (adj_series, n.replace(" ", "_").replace(".", "_"))
        adj_series = "_".join(reversed(adjs[:ind+1])).replace("-","_")
        if adj_series[0].isdigit():
          adj_series = "a%s" % adj_series
        cumulative_oper = "%s_%s_CN = " % (adj_series, n.replace(" ", "_"))
      cumulative_oper = cumulative_oper + "mkCN %s %s ;" % (concrete_func, previous_cumulative_oper_name)
      oper.append(cumulative_oper)
    
    o, oper_noun_name = self._oper_for_noun(n)
    oper.extend(o)
    adj_series = "_".join(natural_order_adjs).replace("-","_")
    if adj_series[0].isdigit():
      adj_series = "a%s" % adj_series
    return oper, "%s_%s_CN" % (adj_series, n.replace(" ", "_"))
    
  def _serialize_noun_compound_and_adj(self, nc_atoms, adj_mod_atoms, main_noun):
    def singularize_noun(n):
      return n if n[0].isupper() else singularize(n)
    
    singular_form = singularize_noun( main_noun )
    if not nc_atoms:
      if not adj_mod_atoms:
        oper, oper_name = self._oper_for_noun(singular_form)
        return oper, "mkNP %s ;" % oper_name
      else:
        oper, oper_name = self._oper_for_adj_noun(adj_mod_atoms, singular_form)
        return oper, "mkNP %s ;" % oper_name
    else: 
      if not adj_mod_atoms:
        nc_atoms = sorted(nc_atoms, key=lambda x: x.args()[0])
        serialized_noun = " ".join(list(map(lambda x: self._tokens[int(x.args()[0]) - 1], nc_atoms)))
        serialized_noun = serialized_noun + " " + singular_form
        oper, oper_name = self._oper_for_noun(serialized_noun)
        return oper, "mkNP %s ;" % oper_name
      else:
        nc_atoms = sorted(nc_atoms, key=lambda x: x.args()[0])
        serialized_noun = " ".join(list(map(lambda x: self._tokens[int(x.args()[0]) - 1], nc_atoms)))
        serialized_noun = serialized_noun + " " + singular_form
        oper, oper_name = self._oper_for_adj_noun(adj_mod_atoms, serialized_noun)
        return oper, "mkNP %s ;" % oper_name
      
  def _serialize_preposition_for_noun(self, preposition_atoms, f):
    oper = []
    func = f
    
    for ind,prep in enumerate(preposition_atoms):
      noun_prep_token = self._tokens[int(prep.args()[0]) - 1]
      
      if not self._dependent_tree[int(prep.args()[0])]:
        o, concrete_func = self._serialize_noun_compound_and_adj([], [], noun_prep_token)
        oper.extend(o)
      else:
        dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(prep.args()[0])]))
        noun_compound_atoms = list(filter(lambda x: x.name() == "noun_compound", dependent_atoms))
        adj_mod_atoms = list(filter(lambda x: x.name() == "adj_mod", dependent_atoms))
        preposition_atoms = list(filter(lambda x: x.name() == "preposition", dependent_atoms))
        
        o, concrete_func = self._serialize_noun_compound_and_adj(noun_compound_atoms, adj_mod_atoms, noun_prep_token)
        oper.extend(o)
        o, concrete_func = self._serialize_preposition_for_noun(preposition_atoms, concrete_func.replace(";",""))
        oper.extend(o)
        
      if self._tokens[int(prep.args()[1])-1] in Constructor._pref_map.keys():
        gf_prep_word = Constructor._pref_map[self._tokens[int(prep.args()[1])-1]]
        prep_block = "(ConstructorsEng.mkAdv %s (%s))" % (gf_prep_word, concrete_func.replace(";",""))
        func = "mkNP (%s) %s" % (func, prep_block)
      else:
        print("Not found preposition \"%s\"" % self._tokens[int(prep.args()[1])-1])
      
      # print(func)
    
    return oper, func
  
  def _serialize_conjunction_for_noun(self, conjunction_atoms, f):
    oper = []
    func = f
    for ind,noun in enumerate(reversed(conjunction_atoms)):
      noun_token = self._tokens[int(noun.args()[0]) - 1]
      
      if not self._dependent_tree[int(noun.args()[0])]:
        o, concrete_func = self._serialize_noun_compound_and_adj([], [], noun_token)
        oper.extend(o)
      else:
        dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(noun.args()[0])]))
        noun_compound_atoms = list(filter(lambda x: x.name() == "noun_compound", dependent_atoms))
        adj_mod_atoms = list(filter(lambda x: x.name() == "adj_mod", dependent_atoms))
        preposition_atoms = list(filter(lambda x: x.name() == "preposition", dependent_atoms))
        
        o, concrete_func = self._serialize_noun_compound_and_adj(noun_compound_atoms, adj_mod_atoms, noun_token)
        oper.extend(o)
        o, concrete_func = self._serialize_preposition_for_noun(preposition_atoms, concrete_func.replace(";",""))
        oper.extend(o)
        
      func = "mkListNP (%s) (%s)" % (concrete_func.replace(";",""), func)

    if conjunction_atoms:
      func = "mkNP and_Conj (%s)" % (func)
      
    return oper, func
    
  def _serialize_preposition_for_verb(self, preposition_atoms, f):
    oper = []
    func = f

    for ind,prep in enumerate(preposition_atoms):
      noun_prep_token = self._tokens[int(prep.args()[0]) - 1]
      
      if not self._dependent_tree[int(prep.args()[0])]:
        o, concrete_func = self._serialize_noun_compound_and_adj([], [], noun_prep_token)
        oper.extend(o)
      else:
        dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(prep.args()[0])]))
        noun_compound_atoms = list(filter(lambda x: x.name() == "noun_compound", dependent_atoms))
        adj_mod_atoms = list(filter(lambda x: x.name() == "adj_mod", dependent_atoms))
        preposition_atoms = list(filter(lambda x: x.name() == "preposition", dependent_atoms))
        
        o, concrete_func = self._serialize_noun_compound_and_adj(noun_compound_atoms, adj_mod_atoms, noun_prep_token)
        oper.extend(o)
        
        o, concrete_func = self._serialize_preposition_for_noun(preposition_atoms, concrete_func.replace(";",""))
        oper.extend(o)
      
      if self._tokens[int(prep.args()[1])-1] in Constructor._pref_map.keys():
        gf_prep_word = Constructor._pref_map[self._tokens[int(prep.args()[1])-1]]
        prep_block = "(ConstructorsEng.mkAdv %s (%s))" % (gf_prep_word, concrete_func.replace(";",""))
        if self._select_structure in [1, 10001]:
          func = "mkVP (%s) %s" % (func, prep_block)
      else:
        print("Not found preposition, use")
    
    return oper, func
  
  def _serialize_verb(self, verb_token):
    lemmatizer = WordNetLemmatizer()
    simple_form = lemmatizer.lemmatize(verb_token, 'v')
    oper, oper_name = self._oper_for_verb(simple_form)
    if self._select_structure == 1:
      return oper, "mkVP %s" % oper_name
    elif self._select_structure == 2:
      return oper, "%s" % oper_name
    elif self._select_structure == 10001:
      return oper, "passiveVP %s" % oper_name

  def _serialize_verb_vv(self, verb_token):
    lemmatizer = WordNetLemmatizer()
    simple_form = lemmatizer.lemmatize(verb_token, 'v')
    oper, oper_name = self._oper_for_verb_vv(simple_form)
    if self._select_structure == 3:
      return oper, "mkVV %s" % oper_name

  def _serialize_verb_v2(self, verb_token):
    lemmatizer = WordNetLemmatizer()
    simple_form = lemmatizer.lemmatize(verb_token, 'v')
    oper, oper_name = self._oper_for_verb_v2(simple_form)
    if self._select_structure == 3:
      return oper, "%s" % oper_name
  
  def _serialize_adj(self, adj_token):
    lemmatizer = WordNetLemmatizer()
    simple_form = lemmatizer.lemmatize(adj_token, pos=wordnet.ADJ)
    oper, oper_name = self._oper_for_adj(simple_form)
    return oper, "%s" % oper_name
  
  def _serialize_adv_adj(self, adverbial_modifier_atoms, adj_token):
    natural_order_advs = list(map(lambda x: self._tokens[int(x.args()[0]) - 1], adverbial_modifier_atoms))
    advs = natural_order_advs[::-1]
    oper = []
        
    for ind,adv in enumerate(reversed(adverbial_modifier_atoms)):
      adv_token = self._tokens[int(adv.args()[0]) - 1]
      o, oper_adv_name = self._oper_for_adv(adv_token)
      oper.extend(o)
      if ind == 0:
        previous_cumulative_oper_name = "%s_AP" % adj_token.replace(" ", "_")
        cumulative_oper = "%s_%s_AP = " % (advs[0].replace("-","_"), adj_token.replace(" ", "_"))
      else:
        previous_cumulative_oper_name = "%s_%s_AP" % ("_".join(advs[:ind]).replace("-","_"), adj_token.replace(" ", "_"))
        cumulative_oper = "%s_%s_AP = " % ("_".join(natural_order_advs[:ind+1]).replace("-","_"), adj_token.replace(" ", "_"))
      cumulative_oper = cumulative_oper + "mkAP %s %s ;" % (oper_adv_name, previous_cumulative_oper_name)
      oper.append(cumulative_oper)
  
    return oper, "%s_%s_AP" % ("_".join(natural_order_advs).replace("-","_"), adj_token.replace(" ", "_"))
      
  def _reconstruct(self,component_atom):
    component_name = component_atom.name()
    component_index = component_atom.args()[0]

    concrete_oper = []
    concrete_func = ""
    
    if component_name in ["sub", "obj"]:
      dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(component_index)]))
      noun_compound_atoms = list(filter(lambda x: x.name() == "noun_compound", dependent_atoms))
      adj_mod_atoms = list(filter(lambda x: x.name() == "adj_mod", dependent_atoms))
      preposition_atoms = list(filter(lambda x: x.name() == "preposition", dependent_atoms))
      number_atoms = list(filter(lambda x: x.name() == "number", dependent_atoms))
      conjunction_atoms = list(filter(lambda x: x.name() == "noun_conjunction", dependent_atoms))

      o, concrete_func = self._serialize_noun_compound_and_adj(noun_compound_atoms, adj_mod_atoms, self._tokens[int(component_index) - 1])
      concrete_oper.extend(o)
      # after this, concrete_func = noun_CN
      
      o, concrete_func = self._serialize_preposition_for_noun(preposition_atoms, concrete_func.replace(";",""))
      concrete_oper.extend(o)
      # after this, concrete_func = noun_NP
      
      o, concrete_func = self._serialize_conjunction_for_noun(conjunction_atoms, concrete_func.replace(";",""))
      concrete_oper.extend(o)
      
    elif component_name in ["verb"]:
      dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(component_index)]))
      preposition_atoms = list(filter(lambda x: x.name() == "preposition", dependent_atoms))

      o, concrete_func = self._serialize_verb(self._tokens[int(component_index) - 1])
      concrete_oper.extend(o)
      # after this, concrete_func = verb_V

      o, concrete_func = self._serialize_preposition_for_verb(preposition_atoms, concrete_func.replace(";",""))
      concrete_oper.extend(o)
    
    elif component_name in ["verb_1"]:
      dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(component_index)]))
      
      o, concrete_func = self._serialize_verb_vv(self._tokens[int(component_index) - 1])
      concrete_oper.extend(o)
      # after this, concrete_func = verb_VV
      
    elif component_name in ["verb_2"]:
      dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(component_index)]))
          
      o, concrete_func = self._serialize_verb_v2(self._tokens[int(component_index) - 1])
      concrete_oper.extend(o)
      # after this, concrete_func = verb_V2
    
    elif component_name in ["adj"]:
      dependent_atoms = list(map(lambda x: Atom(x), self._dependent_tree[int(component_index)]))
      adverbial_modifier_atoms = list(filter(lambda x: x.name() == "adverbial_modifier", dependent_atoms))
      
      o, concrete_func = self._serialize_adj(self._tokens[int(component_index) - 1])
      concrete_oper.extend(o)
      
      if adverbial_modifier_atoms:
        o, concrete_func = self._serialize_adv_adj(adverbial_modifier_atoms, self._tokens[int(component_index) - 1])
        concrete_oper.extend(o)
      
    return concrete_func, concrete_oper
    
  def gf(self):
    def random_string(length):
      return ''.join(random.choice(string.ascii_letters) for m in range(length))
      
    def generate_identifier(affix):
      identifier = random_string(random.randint(4,8))
      return identifier + affix

    lemmatizer = WordNetLemmatizer()

    func = {}
    func["structure"] = self._select_structure 
    func["abstract"] = []
    func["concrete"] = {}
    func["concrete"]["lincat"] = []
    func["concrete"]["lin"] = []    
    func["oper"] = []
    verb_block = ""
    
    if self._select_structure == 1:
      func["abstract"].append("func_structure1 : tbd_NP -> tbd_VP -> Message;")
      
      func["concrete"]["lincat"].append( "tbd_NP = NP ;" )
      func["concrete"]["lincat"].append( "tbd_VP = VP ;" )
      func["concrete"]["lin"].append( "func_structure1 tbd_NP tbd_VP = mkCl tbd_NP tbd_VP ;" )
      verb_block = "VP"
    elif self._select_structure == 2:
      func["abstract"].append("func_structure2 : tbd_sub_NP -> tbd_V2 -> tbd_obj_NP -> Message;")
      
      func["concrete"]["lincat"].append( "tbd_sub_NP = NP ;" )
      func["concrete"]["lincat"].append( "tbd_obj_NP = NP ;" )
      func["concrete"]["lincat"].append( "tbd_V2 = V2 ;" )
      func["concrete"]["lin"].append( "func_structure2 tbd_sub_NP tbd_V2 tbd_obj_NP = mkCl tbd_sub_NP (mkVP tbd_V2 tbd_obj_NP) ;" )
      verb_block = "V2"
    elif self._select_structure == 3:
      func["abstract"].append("func_structure3 : tbd_sub_NP -> tbd_v_VV -> tbd_v_V2 -> tbd_obj_NP -> Message;")
      
      func["concrete"]["lincat"].append( "tbd_sub_NP = NP ;" )
      func["concrete"]["lincat"].append( "tbd_obj_NP = NP ;" )
      func["concrete"]["lincat"].append( "tbd_v_VV = VV ;" )
      func["concrete"]["lincat"].append( "tbd_v_V2 = V2 ;" )
      func["concrete"]["lin"].append( "func_structure3 tbd_sub_NP tbd_v_VV tbd_v_V2 tbd_obj_NP = mkCl tbd_sub_NP tbd_v_VV (mkVP tbd_v_V2 tbd_obj_NP);" )
      verb_block = "VV"
    elif self._select_structure == 101:
      main_comp_atoms = list(map(lambda x: Atom(x), self._main_comps))
      obj_atom = list(filter(lambda x: x.name() == "obj", main_comp_atoms))
      
      if not obj_atom:
        func["abstract"].append("func_structure101 : tbd_sub_NP -> tbd_AP -> Message;")
      
        func["concrete"]["lincat"].append( "tbd_sub_NP = NP ;" )
        func["concrete"]["lincat"].append( "tbd_AP = AP ;" )
        func["concrete"]["lin"].append( "func_structure101 tbd_sub_NP tbd_AP = mkCl tbd_sub_NP tbd_AP;" )
      else:
        func["abstract"].append("func_structure101 : tbd_sub_NP -> tbd_obj_NP -> Message;")
      
        func["concrete"]["lincat"].append( "tbd_sub_NP = NP ;" )
        func["concrete"]["lincat"].append( "tbd_obj_NP = NP ;" )
        func["concrete"]["lin"].append( "func_structure101 tbd_sub_NP tbd_obj_NP = mkCl tbd_sub_NP tbd_obj_NP;" )    
    elif self._select_structure == 10001:
      func["abstract"].append("func_structure10001 : tbd_sub_passive_NP -> tbd_passive_VP -> Message;")
      
      func["concrete"]["lincat"].append( "tbd_sub_passive_NP = NP ;" )
      func["concrete"]["lincat"].append( "tbd_passive_VP = VP ;" )
      func["concrete"]["lin"].append( "func_structure10001 tbd_sub_passive_NP tbd_passive_VP = mkCl tbd_sub_passive_NP tbd_passive_VP ;" )
      verb_block = "passive_VP"
      
    for m in self._main_comps:
      self._build_dependent_tree(m)
    
    self._modify_dependent_tree()
    
    for m in self._main_comps:
      component_atom = Atom(m)
      
      lin, oper = self._reconstruct(component_atom)
      # print(lin)
      # print(oper)
      func["oper"].extend(oper)

      if component_atom.name() in ["sub", "obj"]:
        gf_component_name = generate_identifier("_%s" % component_atom.name())
        if self._select_structure == 1:
          func["abstract"].append( "%s : tbd_NP ;" % (gf_component_name) )
          func["concrete"]["lin"].append("%s = %s ;" % (gf_component_name, lin) )
        elif self._select_structure in [2, 3, 101]:
          func["abstract"].append( "%s : tbd_%s_NP ;" % (gf_component_name, component_atom.name()) )
          func["concrete"]["lin"].append("%s = %s ;" % (gf_component_name, lin) )
        elif self._select_structure in [10001]:
          func["abstract"].append( "%s : tbd_%s_%s_NP ;" % (gf_component_name, component_atom.name(), "passive") )
          func["concrete"]["lin"].append("%s = %s ;" % (gf_component_name, lin) )
          
      elif component_atom.name() in ["verb"]:
        gf_component_name = generate_identifier("_%s" % component_atom.name())
        func["abstract"].append( "%s : tbd_%s ;" % (gf_component_name, verb_block) )
        func["concrete"]["lin"].append("%s = %s ;" % (gf_component_name, lin) )
      
      elif component_atom.name() in ["verb_1"]:
        gf_component_name = generate_identifier("_verb_1_VV")
        func["abstract"].append( "%s : tbd_%s ;" % (gf_component_name, "v_VV") )
        func["concrete"]["lin"].append("%s = %s ;" % (gf_component_name, lin) )
      
      elif component_atom.name() in ["verb_2"]:
        gf_component_name = generate_identifier("_verb_2_V2")
        func["abstract"].append( "%s : tbd_%s ;" % (gf_component_name, "v_V2") )
        func["concrete"]["lin"].append("%s = %s ;" % (gf_component_name, lin) )
      
      elif component_atom.name() in ["adj"]:
        gf_component_name = generate_identifier("_%s" % component_atom.name())
        func["abstract"].append( "%s : tbd_AP ;" % (gf_component_name) )
        func["concrete"]["lin"].append("%s = %s ;" % (gf_component_name, lin) )
      
    print(self._dependent_tree)
    return func
  