import subprocess
import re
from IPython import embed

class Constituency:
  def __init__(self, tree):
    self.tree = tree
  
  def find_best_suit_structure(self, atoms):
    pool = {}
    best = 0
    for a in atoms:
      s = re.findall(r"\w+", a)
      if not s:
        continue
      if s[0] == "structure":
        candidate_match = int(s[2])
        pool[ int(s[1]) ] = candidate_match
        best = candidate_match if candidate_match > best else best
  
    return {k: v for k, v in pool.items() if v == best}
    
  def find_structure(self):
    def labelize(i):
      if self.tree[0,i].label() != ".":
        return "d(%s,%s).\n" % (self.tree[0,i].label().lower(), i)
      else: 
        return ""
      
    facts = list(map(lambda i: labelize(i), range(0,len(self.tree[0]))))
    with open("genoto_v2/temp.lp", "w") as f:
      f.write("".join( facts ))
      
    p = subprocess.Popen('./clingo1facts temp.lp structure.lp', cwd="genoto_v2", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for stdout_line in p.stdout:
      print(stdout_line)
  
    atoms = stdout_line.decode().strip().split(" ")
  
    structure = self.find_best_suit_structure(atoms)
    
    if not structure:
      print( "Can not find a suitable structure for the sentence" )
      return 0
    elif len(structure.keys()) > 1:
      print( "There are multiple structures that can be applied " + " ".join(structure.keys()) )
      select_structure = random.choice(structure.keys())
      print( "Select " + select_structure )
    else:
      select_structure = list(structure.keys())[0]
      
    return select_structure
  
