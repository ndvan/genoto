import re
import subprocess
from IPython import embed

from genoto_v2.constituency import Constituency
from genoto_v2.dependency import Dependency
from genoto_v2.constructor import Constructor

class Genoto:
  
  @staticmethod
  def gfize(constituency, dependency, pos, sentence, tokens):
    # # Another way to find sentence structure but it's not that good because it's too general
    # cons = Constituency(constituency)
    # select_structure = cons.find_structure()
    # if select_structure == 0:
    #   return

    dep = Dependency(dependency, pos)
    select_structure = dep.find_structure()
    
    print("select structure %d" % select_structure)
    
    if select_structure == 0:
      return
    
    main_components = dep.extract_main_components()
    
    # print main_components
    cons = Constructor(select_structure, main_components, dep, pos, tokens)
    func = cons.gf()
    
    return func
    
    
  @staticmethod
  def generate_gfo(funcs):
    available_structure = []
    abstract_syntax = []
    concrete_lin_syntax = []
    concrete_lincat_syntax = []
    oper = []
    list_category = []

    for func in funcs:
      if func is None:
        continue

      oper.extend( func["oper"] )
      if func["structure"] in available_structure:
        abstract_syntax.extend( func["abstract"][1:] )
        concrete_lin_syntax.extend( func["concrete"]["lin"][1:] )
      else:
        available_structure.append( func["structure"] )
        abstract_syntax.extend( func["abstract"] )
        concrete_lin_syntax.extend( func["concrete"]["lin"] )
        concrete_lincat_syntax.extend( func["concrete"]["lincat"] )
        cat = re.findall(r"tbd_\w+", func["abstract"][0])
        list_category.extend( cat )

    with open("gf/Test.gf", "w") as f:
      f.write("abstract Test = {\n")
      f.write("  flags startcat = Message ;\n")
      f.write("cat\n")
      f.write("  Message ; " + "; ".join(list_category) + ";\n")
      f.write("fun\n")
      for a in abstract_syntax:
        f.write("  " + a + "\n")
      f.write("}")

    with open("gf/TestEng.gf", "w") as f:
      f.write("concrete TestEng of Test = open SyntaxEng, ParadigmsEng, ConstructorsEng in {\n")
      f.write("lincat\n")
      f.write("  Message = Cl ;\n")
      for lc in concrete_lincat_syntax:
        f.write("  %s\n" % lc)

      f.write("lin\n")
      for c in concrete_lin_syntax:
        f.write("  " + c + "\n")
      f.write("oper\n")
      for o in oper:
        f.write("  " + o + "\n")
      f.write("}")
    