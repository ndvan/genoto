import subprocess
import re
from IPython import embed

class Atom:
  def __init__(self, a):
    self.str = a
  
  def name(self):
    return re.findall(r"\w+", self.str)[0]
  
  def args(self):
    return re.findall(r"\w+", self.str)[1:]
  
