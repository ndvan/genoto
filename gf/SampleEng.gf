concrete TestEng of Test = open SyntaxEng, ParadigmsEng, ConstructorsEng in {
lincat
  Message = Cl ;
  tbd_NP = NP ;
  tbd_VP = VP ;
lin
  func_structure1 tbd_NP tbd_VP = mkCl tbd_NP tbd_VP ;
  abc = mkNP beautiful_dog_CN ;
  defgh = mkVP eat_V ;
oper
  beautiful_dog_CN = mkCN beautiful_AP dog_CN ;
  dog_CN = mkCN dog_N ;
  dog_N = mkN "dog" "dogs" ;
  beautiful_A = mkA "beautiful" ;
  beautiful_AP = mkAP beautiful_A ;
  
  eat_V = mkV "eat" ;
}