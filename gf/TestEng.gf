concrete TestEng of Test = open SyntaxEng, ParadigmsEng, ConstructorsEng in {
lincat
  Message = Cl ;
  tbd_sub_NP = NP ;
  tbd_obj_NP = NP ;
lin
  func_structure101 tbd_sub_NP tbd_obj_NP = mkCl tbd_sub_NP tbd_obj_NP;
  ABvPYR_sub = mkNP it_N  ;
  QNHWto_obj = mkNP (mkNP consumed_staple_food_CN ) (ConstructorsEng.mkAdv for_Prep (mkNP (mkNP (mkNP large_part_CN ) (ConstructorsEng.mkAdv possess_Prep (mkNP (mkNP human_population_CN ) (ConstructorsEng.mkAdv possess_Prep (mkNP world_N ))))) (ConstructorsEng.mkAdv in_Prep (mkNP Asia_N )))) ;
oper
  it_N = mkN "it" "they" ;
  consumed_A = mkA "consumed" ;
  consumed_AP = mkAP consumed_A ;
  widely_AdA = mkAdA "widely" ;
  widely_consumed_AP = mkAP widely_AdA consumed_AP ;
  consumed_staple_food_CN = mkCN widely_consumed_AP staple_food_N ;
  staple_food_N = mkN "staple food" "staple foods" ;
  cereal_grain_N = mkN "cereal grain" "cereal grains" ;
  large_A = mkA "large" ;
  large_AP = mkAP large_A ;
  large_part_CN = mkCN large_AP part_N ;
  part_N = mkN "part" "parts" ;
  human_A = mkA "human" ;
  human_AP = mkAP human_A ;
  human_population_CN = mkCN human_AP population_N ;
  population_N = mkN "population" "populations" ;
  world_N = mkN "world" "worlds" ;
  Asia_N = mkN "Asia" "Asia" ;
}