from genoto_v2.genoto import Genoto
from nltk.tokenize import sent_tokenize
from nltk.tree import Tree
from nltk.draw.tree import draw_trees
from nltk.parse.corenlp import CoreNLPParser
from nltk.parse.corenlp import CoreNLPDependencyParser
from IPython import embed
import os
import argparse

if __name__ == "__main__":
  funcs = []
  
  # initiate the parser
  parser = argparse.ArgumentParser(description='Build GF grammar')
  parser.add_argument('source', metavar="SOURCE",
                      help='source text')
  args = parser.parse_args()  
  
  # text = "Bill pleases to play piano in the morning. \
#     Bill eats rice. \
#     Bill eats a bowl of rice. \
#     Bill's dog eats a bowl of cereal. \
#     Now it's your turn. \
#     Van waits to drink water at class. \
#     A son of Bill needs to watch TV in the evening. \
#     Bill needs to watch one of his favorite TV shows in the evening. \
#     Sons of Bill also want to eat head of sausage. \
#     Bill eats two bowls of rice.  \
#     A bowl of rice is eaten by Bill. \
#     "
  
  # text = "Bill needs to watch one of popular TV shows in the evening."
  
  # text = "Scores of properties are under extreme fire threat as a huge blaze continues to advance through Sydney's north-western suburbs. \
#     Fires have also shut down the major road and rail links between Sydney and Gosford. \
#     The promotional stop in Sydney was everything to be expected for a Hollywood blockbuster - phalanxes of photographers, a stretch limo to a hotel across the Quay - but with one difference. \
#     A line-up of masseurs was waiting to take the media in hand. \
#     Never has the term \"massaging the media\" seemed so accurate. \
#     "

  # text = "Bill and Van eat a tree. Bill eats two bowls of rice and Van drinks a soda. Jimmy cried when Judy laughed."

  # text = "Modern farming techniques in the nicely beautiful countries usually rely on dense planting."
  # "Bill eats rice in the morning."
  # text = "Bill eats rice in the nicely beautiful morning."
  # text = "Bill eats on the table in the morning."
  # text = "Rice is the seed of the grass species Oryza sativa (Asian rice) or Oryza glaberrima (African rice)."
  # text = "As a cereal grain, it is the most widely consumed staple food for a large part of the world's human population, especially in Asia."
  # text = "It is the agricultural commodity with the third-highest worldwide production (rice, 741.5 million tonnes in 2014), after sugarcane (1.9 billion tonnes) and maize (1.0 billion tonnes)."
  # text = "Rice is usually very good."
  # text = "Rice that has high protein is a seed."
  # text = "The health care provider role and patient are realized during the health care encounter."
  # text = "health care process assay is a processual entity. Processual entity realizes the concretization of a plan specification."
  # text = "The son of my neighbor desperately wants a big two layer vanilla cake on his birthday."

  # text = "In 1986 he graduated from Princeton University with degrees in electrical engineering and computer science. Jeff worked on Wall Street in a variety of fields from 1986 to 1994. He found Amazon in 1994 on a cross-country road trip from New York City to Seattle."
  text = args.source
  sent_tokenize_list = sent_tokenize(text)
  # print(sent_tokenize_list)

  parser = CoreNLPParser()
  pos_parser = CoreNLPParser(tagtype='pos')
  dependency_parser = CoreNLPDependencyParser()
  
  for s in sent_tokenize_list:
    constituency_parse = list(parser.raw_parse(s))
    dependency_parse   = next(dependency_parser.raw_parse(s))

    tokens = list(parser.tokenize(s))
    pos_tagger = list(pos_parser.tag(s.split()))
    
    # embed()
    final_dependency = []
    for k in dependency_parse.nodes.values():
      print(k)
      if k["head"] is not None:
        final_dependency.append(str(k["rel"])  + "(" + tokens[k["head"]-1] + "-" 
          + str(k["head"]) + "," + str(k["word"]) + "-" + str(k["address"]) + ")" )
      # else:
      #   final_dependency.append(str(k["rel"])  + "(" + "Root" + "-"
      #     + str(k["head"]) + "," + str(k["word"]) + "-" + str(k["address"]) + ")" )
    print(final_dependency)
    f = Genoto.gfize(constituency_parse, final_dependency, pos_tagger, s, tokens)

    funcs.append(f)
    # print("\n")
  # print(funcs)
  Genoto.generate_gfo(funcs)
