from genoto.parser import Parser
from genoto.clex_generator import ClexGenerator
from genoto.verb import Verb
from genoto.adjective import Adj
import subprocess
import xml.etree.ElementTree

if __name__ == "__main__":
    x = Parser(".")
    files = x.files("txt")
    all_nouns      = []
    all_pronouns   = []
    all_verbs      = []
    all_adjectives = []
    
    for f in files:
        print f
        nouns, pronouns = x.extract(f)
        # for a in nouns:
        #     print a.sg, a.pl
        # for a in pronouns:
        #     print a.sg
        
        all_nouns.extend(nouns)
        all_pronouns.extend(pronouns)
        
    cl = ClexGenerator(all_nouns, all_pronouns, [], [])
    
    print "Generating clex (lexicon) file"
    cl.gen()
    print "Done"
    
    print "Compile Attempto Parser"
    p = subprocess.Popen('sh make_exe.sh', cwd='APE', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in iter(p.stdout.readline, ''):
        print line
    print "Done"

    # p = subprocess.Popen('./ape.exe -text "A beautiful program of Apple-Inc comes-from Beats." -cdrsxml -csyntaxpp -guess', cwd='APE', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    
    comments = ["Beats produces a headphone.", 
                "A beautiful program of Apple-Inc agrees Beats."]
    
    for com in comments:
        p = subprocess.Popen('./ape.exe -text "' + com + '" -cdrsxml -csyntaxpp -guess', cwd='APE', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out = ""
        for line in iter(p.stdout.readline, ''):
            out += line
            print line

        out_xml = xml.etree.ElementTree.XML(out)
        for elem in out_xml.iter():
            if elem.tag == "drsxml":
                drs_text = elem.text
                drs_xml = xml.etree.ElementTree.XML(drs_text)
                for e in drs_xml.iter():
                    print e.tag, e.attrib
                    if e.tag == "predicate":
                        for key, val in e.attrib.iteritems():
                            if key == "verb":
                                print "Found verb " + val
                                v = Verb(val)
                                # print v.t, v.i, v.d
                                # print v.finite_singular
                                all_verbs.append(v)
                            
                    elif e.tag == "property":
                        for k,w in e.attrib.iteritems():
                            if k == "adj":
                                print "Found adjective " + w
                                adj = Adj(w)
                                # print adj.pos, adj.com, adj.sup
                                all_adjectives.append(adj)
                            
    cl = ClexGenerator(all_nouns, all_pronouns, all_verbs, all_adjectives)
    
    print "Completing clex file"
    cl.gen("words/clex.pl")
    print "Done"
    
    print "Generate clex interface file"
    cl.gen_interface("words/TestAttemptoI.gf")
    print "Done"
    
    print "Converting clex to GF"
    p = subprocess.Popen("swipl -f none -g \"main('clex.pl')\" -t halt -s clex_to_gf.pl", cwd='words', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in iter(p.stdout.readline, ''):
        print line
    print "Done"